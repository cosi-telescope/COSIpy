FROM  python:3.6

# === General dependencies ===
RUN apt-get update && apt-get install -y pandoc
RUN pip install nbsphinx sphinx-rtd-theme
RUN pip install pytest-cov

